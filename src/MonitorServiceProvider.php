<?php

namespace Shizzen\Monitor;

use Shizzen\JWTAuth\Events\JWTLogout;
use Shizzen\JWTAuth\Events\JWTInvalidate;
use Shizzen\JWTAuth\Events\JWTRefresh;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Broadcasting\BroadcastManager;
use Illuminate\Broadcasting\BroadcastController;
use Illuminate\Contracts\Events\Dispatcher as EventDispatcher;

class MonitorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JWTSubjectObserver::class);
        $this->app->singleton(Middleware\BroadcastTerminator::class);

        $this->app->resolving(BroadcastController::class, function ($controller, $app) {
            $controller->middleware('broadcast.terminator');
        });

        $this->app->resolving(BroadcastManager::class, function ($manager, $app) {
            $manager->extend('redis', function ($app, $config) {
                return new RedisBroadcaster(
                    $app['redis'], $config['connection'] ?? null,
                    $app['config']->get('database.redis.options.prefix', '')
                );
            });
        });
    }

    /**
     * Bootstrap services.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(EventDispatcher $events, Router $router)
    {
        $events->listen([JWTLogout::class, JWTInvalidate::class], Listeners\PublishInvalidate::class);

        $events->listen(JWTRefresh::class, Listeners\PublishRefresh::class);

        $router->middlewareGroup('broadcast.terminator', [Middleware\BroadcastTerminator::class]);
    }
}
