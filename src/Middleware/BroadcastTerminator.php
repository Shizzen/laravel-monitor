<?php

namespace Shizzen\Monitor\Middleware;

use Closure;
use Shizzen\JWTAuth\Claims\Expiration;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Contracts\Broadcasting\Factory as BroadcastingFactory;
use Shizzen\Monitor\Events\BroadcastDenied;
use Shizzen\Monitor\Events\BroadcastAllowed;

class BroadcastTerminator
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * The events dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    protected $events;

    /**
     * The broadcasting factory instance.
     *
     * @var \Illuminate\Contracts\Broadcasting\Factory
     */
    protected $broadcastingFactory;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @param  \Illuminate\Contracts\Broadcasting\Factory  $broadcastingFactory
     * @return void
     */
    public function __construct(Auth $auth, Dispatcher $events, BroadcastingFactory $broadcastingFactory)
    {
        $this->auth = $auth;
        $this->events = $events;
        $this->broadcastingFactory = $broadcastingFactory;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)->withHeaders([
            'token' => (string) $this->getToken(),
        ]);
    }

    /**
     * Handle the post-request process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $user = $request->user();

        $fullChannel = $request->input('channel_name');
        [$channelType, $channelName] = explode('-', $fullChannel, 2);

        if ($response->isSuccessful()) {
            $this->events->dispatch(
                new BroadcastAllowed($user, $channelType, $channelName, $this->broadcastingFactory->socket(), $this->getToken())
            );
        }
        else {
            $this->events->dispatch(new BroadcastDenied($user, $channelType, $channelName));
        }
    }

    /**
     * Get the request token.
     *
     * @return \Shizzen\JWTAuth\Token
     */
    protected function getToken()
    {
        return $this->auth->guard()->getToken();
    }
}
