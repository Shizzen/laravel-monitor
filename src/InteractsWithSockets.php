<?php

namespace Shizzen\Monitor;

use Illuminate\Contracts\Broadcasting\Factory as BroadcastingFactoryContract;
use Illuminate\Broadcasting\InteractsWithSockets as BaseInteractsWithSockets;

trait InteractsWithSockets
{
    use BaseInteractsWithSockets;

    /**
     *
     * Whether the socket is the only target or is excluded.
     *
     * @var bool
     */
    public $include = false;

    /**
     * Broadcast to only/except a given socket.
     *
     * @param  string  $socket
     * @param  bool  $include
     * @return $this
     */
    public function broadcastToSocket(string $socket, bool $include)
    {
        $this->socket = $socket;
        $this->include = $include;

        return $this;
    }

    /**
     * Broadcast only to the current user.
     *
     * @return $this
     */
    public function onlyBroadcastToCurrentUser()
    {
        return $this->broadcastToSocket($this->getCurrentSocket(), true);
    }

    /**
     * Exclude the current user from receiving the broadcast.
     *
     * @return $this
     */
    public function dontBroadcastToCurrentUser()
    {
        return $this->broadcastToSocket($this->getCurrentSocket(), false);
    }

    /**
     * Retrieve the current user socket.
     *
     * @return string
     */
    public function getCurrentSocket()
    {
        return app(BroadcastingFactoryContract::class)->socket();
    }
}
