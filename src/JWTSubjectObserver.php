<?php

namespace Shizzen\Monitor;

use Illuminate\Redis\Connections\Connection as Redis;
use Shizzen\JWTAuth\Claims;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\JWT;

class JWTSubjectObserver
{
    /**
     * The Redis connection instance.
     *
     * @var \Illuminate\Redis\Connections\Connection
     */
    protected $redis;

    /**
     * The JWT payload factory.
     *
     * @var \Shizzen\JWTAuth\JWT
     */
    protected $jwt;

    /**
     * Create the observer.
     *
     * @param  \Illuminate\Redis\Connections\Connection  $redis
     * @param  \Shizzen\JWTAuth\JWT  $jwt
     * @return void
     */
    public function __construct(Redis $redis, JWT $jwt)
    {
        $this->redis = $redis;
        $this->jwt = $jwt;
    }

    /**
     * Handle the instance "deleted" event.
     *
     * @param  \Shizzen\JWTAuth\Contracts\JWTSubject  $subject
     * @return void
     */
    public function deleted(JWTSubject $subject)
    {
        $payload = $this->jwt->fromSubject($subject)->payload();

        $this->redis->publish('pmessage', json_encode([
            'event' => 'delete',
            'hsu'   => $payload->get(Claims\HashedSubject::NAME),
            'sub'   => $payload->get(Claims\Subject::NAME),
        ]));
    }
}
