<?php

namespace Shizzen\Monitor\Listeners;

use Shizzen\JWTAuth\Events\JWTEvent;
use Illuminate\Redis\Connections\Connection as Redis;

class PublishInvalidate
{
    /**
     * The Redis connection instance.
     *
     * @var \Illuminate\Redis\Connections\Connection
     */
    protected $redis;

    /**
     * Create the event listener.
     *
     * @param  \Illuminate\Redis\Connections\Connection  $redis
     * @return void
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Handle the event.
     *
     * @param  \Shizzen\JWTAuth\Events\JWTEvent  $event
     * @return void
     */
    public function handle(JWTEvent $event)
    {
        $this->redis->publish('pmessage', json_encode([
            'event' => 'invalidate',
            'token' => (string) $event->token,
        ]));
    }
}
