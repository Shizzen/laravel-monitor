<?php

namespace Shizzen\Monitor\Listeners;

use Shizzen\JWTAuth\Events\JWTRefresh;
use Illuminate\Redis\Connections\Connection as Redis;

class PublishRefresh
{
    /**
     * The Redis connection instance.
     *
     * @var \Illuminate\Redis\Connections\Connection
     */
    protected $redis;

    /**
     * Create the event listener.
     *
     * @param  \Illuminate\Redis\Connections\Connection  $redis
     * @return void
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Handle the event.
     *
     * @param  \Shizzen\JWTAuth\Events\JWTRefresh  $event
     */
    public function handle(JWTRefresh $event)
    {
        $this->redis->publish('pmessage', json_encode([
            'event'     => 'refresh',
            'oldToken'  => (string) $event->oldToken,
            'newToken'  => (string) $event->token,
        ]));
    }
}
