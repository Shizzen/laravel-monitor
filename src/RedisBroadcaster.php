<?php

namespace Shizzen\Monitor;

use Illuminate\Broadcasting\Broadcasters\RedisBroadcaster as BaseRedisBroadcaster;

class RedisBroadcaster extends BaseRedisBroadcaster
{
    /**
     * Return the valid authentication response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $result
     * @return mixed
     */
    public function validAuthenticationResponse($request, $result)
    {
        if (is_bool($result)) {
            return json_encode($result);
        }

        [$channelType, $channelName] = explode('-', $request->input('channel_name'), 2);

        $user = $this->retrieveUser($request, $channelName);

        $channel_data = [
            'user_type' => get_class($user),
            'user_id' => $user->getAuthIdentifier(),
            'user_info' => $result,
        ];

        if ($channelType === 'presence') {
            $channel_data['private'] = method_exists($user, 'isPrivateOnPresenceChannel')
                ? $user->isPrivateOnPresenceChannel($channelName)
                : false;
        }

        return json_encode(compact('channel_data'));
    }
}
