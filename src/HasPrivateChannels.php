<?php

namespace Shizzen\Monitor;

trait HasPrivateChannels
{
    /**
     * The bootstrapper of this trait (automatically called).
     *
     * @return void
     */
    protected static function bootHasPrivateChannels()
    {
        static::observe(JWTSubjectObserver::class);
    }
}
