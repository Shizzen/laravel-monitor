<?php

namespace Shizzen\Monitor\Events;

use Illuminate\Contracts\Auth\Authenticatable;

class BroadcastDenied
{
    /**
     * User denied.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public $user;

    /**
     * Whether the channel is private or presence.
     *
     * @var string
     */
    public $channelType;

    /**
     * Name of the denied channel.
     *
     * @var string
     */
    public $channelName;

    /**
     * Create a new event instance.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable|null  $user
     * @param  string  $channelType
     * @param  string  $channelName
     * @return void
     */
    public function __construct(?Authenticatable $user, string $channelType, string $channelName)
    {
        $this->user = $user;
        $this->channelType = $channelType;
        $this->channelName = $channelName;
    }
}
