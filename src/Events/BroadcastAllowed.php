<?php

namespace Shizzen\Monitor\Events;

use Shizzen\JWTAuth\Token;
use Illuminate\Contracts\Auth\Authenticatable;

class BroadcastAllowed
{
    /**
     * User allowed.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    public $user;

    /**
     * Whether the channel is private or presence.
     *
     * @var string
     */
    public $channelType;

    /**
     * Name of the allowed channel.
     *
     * @var string
     */
    public $channelName;

    /**
     * The broadcasted socket.
     *
     * @var string
     */
    public $socket;

    /**
     * Token used to authenticate the user.
     *
     * @var \Shizzen\JWTAuth\Token
     */
    public $token;

    /**
     * Create a new event instance.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $channelType
     * @param  string  $channelName
     * @param  string  $socket
     * @param  \Shizzen\JWTAuth\Token  $token
     * @return void
     */
    public function __construct(Authenticatable $user, string $channelType, string $channelName, string $socket, Token $token)
    {
        $this->user = $user;
        $this->channelType = $channelType;
        $this->channelName = $channelName;
        $this->socket = $socket;
        $this->token = $token;
    }
}
