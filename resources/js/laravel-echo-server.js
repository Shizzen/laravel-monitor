#!/usr/bin/env node

const _ = require('lodash');
const url = require('url');

const entrypoint = require.resolve('laravel-echo-server');

const EchoServer = require(`${entrypoint}/../echo-server.js`).EchoServer;
const HttpApi = require(`${entrypoint}/../api/http-api.js`).HttpApi;
const Channel = require(`${entrypoint}/../channels/channel.js`).Channel;
const PrivateChannel = require(`${entrypoint}/../channels/private-channel.js`).PrivateChannel;
const PresenceChannel = require(`${entrypoint}/../channels/presence-channel.js`).PresenceChannel;

const log_1 = require(`${entrypoint}/../log.js`);

const usersComparator = (user1, user2) => user1.user_id === user2.user_id && user1.user_type === user2.user_type;

const tokensTracker = {};

const initTokenTracking = (hsu, sub, jti, exp = null, callbacks = {}) => {
    tokensTracker[hsu] = tokensTracker[hsu] || {};
    tokensTracker[hsu][sub] = tokensTracker[hsu][sub] || {};
    tokensTracker[hsu][sub][jti] = tokensTracker[hsu][sub][jti] || { leaveTask: null, callbacks };

    if (exp) {
        tokensTracker[hsu][sub][jti].leaveTask = tokensTracker[hsu][sub][jti].leaveTask || setTimeout(() => {
            tokenCallback(hsu, sub, jti);
        }, exp * 1000 - Date.now());
    }

    return tokensTracker[hsu][sub][jti];
};

const tokenCallback = (hsu, sub, jti) => {
    const callbacks = Object.values(tokensTracker[hsu][sub][jti].callbacks).map(Object.values).flat();

    callbacks.forEach(callback => callback());

    delete tokensTracker[hsu][sub][jti];
};

const parseTokenPayload = (token) => {
    const buffer = Buffer.from(token.split('.')[1], 'base64');
    return JSON.parse(buffer.toString('ascii'));
};

EchoServer.prototype.listen = function () {
    var _this = this;
    return new Promise(function (resolve, reject) {
        var subscribePromises = _this.subscribers.map(function (subscriber) {
            return subscriber.subscribe(function (channel, message) {
                switch (message.event) {
                    case 'invalidate':
                        var { hsu, sub, jti } = parseTokenPayload(message.token);

                        clearTimeout(tokensTracker[hsu][sub][jti].leaveTask);
                        tokenCallback(hsu, sub, jti);
                        break;
                    case 'refresh':
                        var { hsu: oldHsu, sub: oldSub, jti: oldJti } = parseTokenPayload(message.oldToken);
                        var { hsu: newHsu, sub: newSub, jti: newJti, exp = null } = parseTokenPayload(message.newToken);

                        clearTimeout(tokensTracker[oldHsu][oldSub][oldJti].leaveTask);

                        initTokenTracking(newHsu, newSub, newJti, exp, tokensTracker[oldHsu][oldSub][oldJti].callbacks);

                        delete tokensTracker[oldHsu][oldSub][oldJti];
                        break;
                    case 'delete':
                        var { hsu, sub } = message;

                        Object.entries(tokensTracker[hsu][sub]).forEach(([jti, tokenData]) => {
                            clearTimeout(tokenData.leaveTask);
                            tokenCallback(hsu, sub, jti);
                        });
                        delete tokensTracker[hsu][sub];
                        break;
                    default:
                        return _this.broadcast(channel, message);
                }
            });
        });
        Promise.all(subscribePromises).then(function () { return resolve(); });
    });
};

EchoServer.prototype.broadcast = function (channel, message) {
    if (message.socket && this.find(message.socket)) {
        if (_.get(message, 'include', false)) {
            return this.toOne(this.find(message.socket), channel, message);
        }
        return this.toOthers(this.find(message.socket), channel, message);
    }
    else {
        return this.toAll(channel, message);
    }
};

//non-existing
EchoServer.prototype.toOne = function (socket, channel, message) {
    socket.to(channel)
        .emit(message.event, channel, message.data);
    return true;
};

HttpApi.prototype.getChannel = function (req, res) {
    var channelName = req.params.channelName;
    var room = this.io.sockets.adapter.rooms[channelName];
    var subscriptionCount = room ? room.length : 0;
    var result = {
        subscription_count: subscriptionCount,
        occupied: !!subscriptionCount
    };
    if (this.channel.isPresence(channelName)) {
        this.channel.presence.getMembers(channelName).then(function (members) {
            result['user_count'] = _.uniqWith(members, usersComparator).length;
            res.json(result);
        });
    }
    else {
        res.json(result);
    }
};

HttpApi.prototype.getChannelUsers = function (req, res) {
    var channelName = req.params.channelName;
    if (!this.channel.isPresence(channelName)) {
        return this.badResponse(req, res, 'User list is only possible for Presence Channels');
    }
    this.channel.presence.getMembers(channelName).then(function (members) {
        var users = [];
        _.uniqWith(members, usersComparator).forEach(function (member) {
            users.push({ type: member.user_type, id: member.user_id, user_info: member.user_info });
        });
        res.json({ users: users });
    }, function (error) { return log_1.Log.error(error); });
};

Channel.prototype.joinPrivate = function (socket, data) {
    var _this = this;
    this.private.authenticate(socket, data).then(function ([headers, res]) {
        socket.join(data.channel);
        if (_this.isPresence(data.channel)) {
            var member = res.channel_data;
            try {
                member = JSON.parse(res.channel_data);
            }
            catch (e) { }
            _this.presence.join(socket, data.channel, member);
        }

        var { hsu, sub, jti, exp = null } = parseTokenPayload(headers.token);

        var channelCallbacks = initTokenTracking(hsu, sub, jti, exp).callbacks;

        channelCallbacks[socket.id] = channelCallbacks[socket.id] || {};
        channelCallbacks[socket.id][data.channel] = channelCallbacks[socket.id][data.channel] || function () {
            if (Object.keys(socket.rooms).includes(data.channel)) {
                this.leave(socket, data.channel, 'Expired token');
            }
        }.bind(_this);

        _this.onJoin(socket, data.channel);
    }, function (error) {
        if (_this.options.devMode) {
            log_1.Log.error(error.reason);
        }
        _this.io.sockets.to(socket.id)
            .emit('subscription_error', data.channel, error.status);
    });
};

const basePrivateAuthenticate = PrivateChannel.prototype.authenticate;
PrivateChannel.prototype.authenticate = function (socket, data) {
    _.set(data, 'auth.headers.X-Socket-ID', socket.id);

    return basePrivateAuthenticate.bind(this)(socket, data);
};

PrivateChannel.prototype.authHost = function (socket) {
    var authHosts = (this.options.authHost) ?
        this.options.authHost : this.options.host;
    if (typeof authHosts === "string") {
        authHosts = [authHosts];
    }
    var authHostSelected = authHosts[0] || 'http://localhost';
    if (socket.request.headers.referer) {
        var referer = url.parse(socket.request.headers.referer);
        for (var _i = 0, authHosts_1 = authHosts; _i < authHosts_1.length; _i++) {
            var authHost = authHosts_1[_i];
            authHostSelected = authHost;
            if (this.hasMatchingHost(referer, authHost)) {
                authHostSelected = referer.protocol + "//" + referer.host;
                break;
            }
        }
        ;
    }

    authHostSelected = authHostSelected.replace('*', referer.hostname);

    if (this.options.devMode) {
        log_1.Log.error("[" + new Date().toLocaleTimeString() + "] - Preparing authentication request to: " + authHostSelected);
    }
    return authHostSelected;
};

PrivateChannel.prototype.serverRequest = function (socket, options) {
    var _this = this;
    return new Promise(function (resolve, reject) {
        options.headers = _this.prepareHeaders(socket, options);
        var body;
        _this.request.post(options, function (error, response, body, next) {
            if (error) {
                if (_this.options.devMode) {
                    log_1.Log.error("[" + new Date().toLocaleTimeString() + "] - Error authenticating " + socket.id + " for " + options.form.channel_name);
                    log_1.Log.error(error);
                }
                reject({ reason: 'Error sending authentication request.', status: 0 });
            }
            else if (response.statusCode !== 200) {
                if (_this.options.devMode) {
                    log_1.Log.warning("[" + new Date().toLocaleTimeString() + "] - " + socket.id + " could not be authenticated to " + options.form.channel_name);
                    log_1.Log.error(response.body);
                }
                reject({ reason: 'Client can not be authenticated, got HTTP status ' + response.statusCode, status: response.statusCode });
            }
            else {
                if (_this.options.devMode) {
                    log_1.Log.info("[" + new Date().toLocaleTimeString() + "] - " + socket.id + " authenticated for: " + options.form.channel_name);
                }
                try {
                    body = JSON.parse(response.body);
                }
                catch (e) {
                    body = response.body;
                }
                resolve([response.headers, body]);
            }
        });
    });
};

//non-existing
PresenceChannel.prototype.isPrivateMember = function (member) {
    return member.private;
};

PresenceChannel.prototype.isMember = function (channel, member) {
    var _this = this;
    return new Promise(function (resolve, reject) {
        _this.getMembers(channel).then(function (members) {
            _this.removeInactive(channel, members, member).then(function (members) {
                var search = members.filter(m => usersComparator(m, member));
                if (search && search.length) {
                    resolve(true);
                }
                resolve(false);
            });
        }, function (error) { return log_1.Log.error(error); });
    });
};

PresenceChannel.prototype.join = function (socket, channel, member) {
        var _this = this;
        if (!member) {
            if (this.options.devMode) {
                log_1.Log.error("Unable to join channel. Member data for presence channel missing");
            }
            return;
        }
        this.isMember(channel, member).then(function (is_member) {
            _this.getMembers(channel).then(function (members) {
                members = members || [];
                member.socketId = socket.id;
                members.push(member);
                _this.db.set(channel + ":members", members);
                members = _.uniqWith(members.reverse(), usersComparator);
                _this.onSubscribed(socket, channel, members);
                if (!is_member) {
                    _this.onJoin(socket, channel, member);
                }
            }, function (error) { return log_1.Log.error(error); });
        }, function () {
            log_1.Log.error("Error retrieving presence channel members.");
        });
    };

const basePresenceOnJoin = PresenceChannel.prototype.onJoin;
PresenceChannel.prototype.onJoin = function (socket, channel, member) {
    if (!this.isPrivateMember(member)) {
        basePresenceOnJoin.bind(this)(socket, channel, member);
    }
};

const basePresenceOnLeave = PresenceChannel.prototype.onLeave;
PresenceChannel.prototype.onLeave = function (channel, member) {
    if (!this.isPrivateMember(member)) {
        basePresenceOnLeave.bind(this)(channel, member);
    }
};

const basePresenceOnSubscribed = PresenceChannel.prototype.onSubscribed;
PresenceChannel.prototype.onSubscribed = function (socket, channel, members) {
    basePresenceOnSubscribed.bind(this)(socket, channel, _.reject(members, this.isPrivateMember));
};


require(`${entrypoint}/../cli/index.js`);
